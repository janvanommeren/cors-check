#!/usr/bin/python

from urlparse import urlparse
import sys, requests

if sys.argv[1] is None:
	print 'No domain supplied.'
	exit()

domain = urlparse(sys.argv[1])

useCredentials = False
headers = {'Origin': domain.netloc}
response = requests.get(domain.geturl(), headers=headers)
if 'Access-Control-Allow-Credentials' in response.headers:
	if response.headers['Access-Control-Allow-Credentials'] == 'true':
		useCredentials = True
if 'Access-Control-Allow-Origin' in response.headers:
	corsHeader = response.headers['Access-Control-Allow-Origin']
	if domain.netloc in corsHeader or 'null' in corsHeader and useCredentials:
		print domain.geturl() + '\tHigh\tCORS'
	elif domain.netloc in corsHeader or 'null' in corsHeader or '*' in corsHeader:
		if 'Access-Control-Allow-Headers' in response.headers and 'Vary' not in response.headers:
			wildcardHeaders = {}
			payload = 's0meth1ngR4nd0m'
			for header in response.headers['Access-Control-Allow-Headers'].split(','):
				wildcardHeaders[header.strip()] = payload
			wildcardResponse = requests.get(domain.geturl(), headers=wildcardHeaders)
			if payload in wildcardResponse.content:
				print domain.geturl() + '\tMedium\tClient Side Cache Poisoning'
			else:
				print domain.geturl() + '\tInfo\tPossible CSCP'